<?php
/**
 * @file
 * Antispammer bot admin module.
 */

/**
 * Admin settings.
 */
function antispammer_bot_settings() {

  $form['antispammer_bot_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate antispammer bot?'),
    '#default_value' => variable_get('antispammer_bot_active', 0),
    '#description' => t('Activate antispammer bot.'),
  );

  $form['antispammer_bot_peruser_nodes_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Maximum posts per non safe user'),
    '#default_value' => variable_get('antispammer_bot_peruser_nodes_threshold', 4),
    '#options' => array(range(0, 400)),
    '#description' => t('Limit of contents (nodes) that a non safe user can publish.'),
  );

  $form['antispammer_bot_peruser_comments_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Maximum comments per non safe user'),
    '#default_value' => variable_get('antispammer_bot_peruser_comments_threshold', 4),
    '#options' => array(range(0, 400)),
    '#description' => t('Limit of contents (comments) that a non safe user can publish.'),
  );

  $form['antispammer_bot_peruser_seconds_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Minimum of seconds between posts'),
    '#default_value' => variable_get('antispammer_bot_peruser_seconds_threshold', 60),
    '#options' => array(range(0, 60)),
    '#description' => t('Minimum of seconds between posts to flag as non safe user.'),
  );

  $form['antispammer_bot_peruser_message'] = array(
    '#type' => 'markup',
    '#value' => t('<p>Note: Bypass this limitations selecting "safe roles" in your user permissions</p>'),
  );

  return system_settings_form($form);
}
