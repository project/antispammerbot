<?php
/**
 * @file
 * Antispammer bot module, inc file with non-hook functions.
 */

/**
 * Helper function to check limit violations in a node. Always FALSE for uid1.
 *
 * @param array $node
 *   Node for which we will check the permissions
 *
 * @return bool
 *   return if violates or not the limits
 */
function _antispammer_bot_violates_limits($node) {
  // Responsible user.
  global $user;
  // Innocent until shown otherwise.
  $possible_spammer = FALSE;

  if (antispammer_bot_is_active()) {
    // Limit cheks. First, time.
    $violates_time_limits = _antispammer_bot_violates_time_limit($node);
    // Violates amount limits? (usefull for massive spammers).
    $violates_amount_limits = _antispammer_bot_violates_amount_limit($node);

    // Violates amount limits for comments? (usefull for massive spammers).
    $violates_comment_limits = _antispammer_bot_violates_comment_amount_limit($node);

    // If the user violates any limit, it is a possible spammer.
    if (($violates_time_limits) || ($violates_amount_limits) || ($violates_comment_limits)) {
      $possible_spammer = TRUE;
    }

    // If violates time limits and is not the admin.
    if ($violates_time_limits && $user->uid != 1) {
      // We lock this account, as it seems unsafe user publishing too fast.
      user_block_user_action($user);
    }
  }

  // TODO //.
  // If VIOLATES LIMITS, 1) block user, 2) unpublish posts and 3) send email.
  // TODO.
  return $possible_spammer;
}

/**
 * Return if the bot is active and can be aplied (user is unsafe, etc...).
 *
 * @return bool
 *   return if bot is active
 */
function antispammer_bot_is_active() {
  // Is bot active?
  $bot_is_active = FALSE;

  // Check if the Antispam bot is activated by admin.
  $bot_admin_active = variable_get('antispammer_bot_active', 0);
  // Do not continue unless it is active.
  if ($bot_admin_active) {
    // If the user has permissions, he has no limits.
    if (!user_access('bypass antispammer bot limits')) {
      $bot_is_active = TRUE;
    }
  }

  return $bot_is_active;
}

/**
 * Helper function to check time limit violations for this node. 1 for uid=1.
 * 
 * @param array $node
 *   The node to check.
 * 
 * @return bool
 *   returns if violates time limits
 */
function _antispammer_bot_violates_time_limit($node) {
  $violates_limit = FALSE;

  // Threshold time limit (minutes).
  $limit_seconds = variable_get('antispammer_bot_peruser_seconds_threshold', 60);

  // Current time.
  $today = new DateTime(
    format_date(_antispammer_bot_latest_post_date($node->uid), 'custom', 'd M Y H:i:s'));
  // Date to which we want to compare (A Drupal field in my case).
  $appt = new DateTime(format_date($node->created, 'custom', 'd M Y H:i:s'));

  // Months since last post.
  $months_since_appt = $appt->diff($today)->m;
  // Days since last post.
  $days_since_appt = $appt->diff($today)->days;
  // Hours since last post.
  $hours_since_appt = $appt->diff($today)->h;
  // Minutes since last post.
  $minutes_since_appt = $appt->diff($today)->i;
  // Minutes since last post.
  $seconds_since_appt = $appt->diff($today)->s;

  // Total seconds.
  $total_seconds = $minutes_since_appt * 60 + $seconds_since_appt;

  // TODO: MOVE TIME LIMITS TO ADMIN INTERFACE.
  // Check if violates limits.
  if (($months_since_appt == 0) && ($days_since_appt == 0) && ($hours_since_appt == 0)
    && ($total_seconds < $limit_seconds)) {
    $violates_limit = TRUE;
  }

  return $violates_limit;
}

/**
 * Helper function to check amount limit violations for this node. 1 for uid:1.
 * 
 * @param array $node
 *   node to check
 * 
 *   return if violates amount limits
 */
function _antispammer_bot_violates_amount_limit($node) {
  $violates_limit = FALSE;

  // Amount threshold (nodes published).
  $amount_threshold = variable_get('antispammer_bot_peruser_nodes_threshold', 4);

  // Number of posts published by this user so far.
  $num_posts = _antispammer_bot_num_posts($node->uid);

  if ($num_posts > $amount_threshold) {
    $violates_limit = TRUE;
  }

  return $violates_limit;
}

/**
 * Helper function to check amount limit violations for comments. FALSE in uid1.
 *
 * @param array $comment
 *   comment node to check limits
 * 
 * @return bool
 *   follow or not the limits
 */
function _antispammer_bot_violates_comment_amount_limit($comment) {
  $violates_limit = FALSE;
  // Current user trying to publish the comment.
  global $user;

  // Limit of comments for non safe users.
  $comments_limit
    = variable_get('antispammer_bot_peruser_comments_threshold', 4);

  // Number of comments for current user.
  $num_comments = _antispammer_bot_num_comments($user->uid);

  if ($num_comments > $comments_limit) {
    $violates_limit = TRUE;
  }

  // Hook_comment WILL HELP, check validate $op.
  return $violates_limit;
}

/**
 * Return date of the last created node for a given user uid.
 * 
 * @param int $uid
 *   User id
 * 
 * @return date
 *   Date of the latest published post
 */
function _antispammer_bot_latest_post_date($uid) {

  $last_node = db_query("SELECT created FROM {node} WHERE uid = %d 
    ORDER BY created DESC", array(":uid" => $uid))->fetch_assoc();

  return $last_node['created'];
}

/**
 * Return num of nodes created by user uid.
 * 
 * @param array $uid
 *   uid of the user to check
 * 
 * @return int
 *   Number of nodes published by the user 
 */
function _antispammer_bot_num_posts($uid) {

  $num_nodes = db_query("SELECT COUNT(nid) FROM {node} WHERE uid = %d 
    ORDER BY created DESC", array(":uid" => $uid))->fetch_assoc();

  return $num_nodes['COUNT(nid)'];
}

/**
 * Return num of comments created by user uid.
 * 
 * @param array $uid
 *   uid of the user to check
 * 
 * @return int
 *   Number of nodes published by the user
 */
function _antispammer_bot_num_comments($uid) {

  $num_nodes = db_query("SELECT COUNT(cid) FROM {comments} WHERE uid = %d",
    array(":uid" => $uid))->fetch_assoc();

  return $num_nodes['COUNT(cid)'];
}
