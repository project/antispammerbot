Installation
git clone --branch 6.x-1.x 
http://git.drupal.org/sandbox/programadoresweb/2062343.git antispammer_bot

Motivation
My main motivation to write this module was having thousands of comments and 
nodes published after a week off. What it is worst, a single spammer can affect
 your server, even force it down because of publishing some millions of 
comments/nodes in your site.

Said that, this is Not Just Another Spam Module which detects spam, but a bot
 itself which detects if the user is a possible spammer and take some actions, 
like:

block the user account
un-publish all the contents of this user
notify the site administrator
All blocked users will be displayed in a separate page, where the user can 
moderate these users.

Alternatively the time frame can be modified, add a "secure" role which will be 
able to publish more than X posts defined as possible offender (more than 
10/100/X per day/week/month/total)...

New users can also have a threshold beyond which the system will automatically 
unpublish their posts and block the user himself. That way, for example, any 
new user publishing his first post will be blocked, posts unpublished and moved
into moderation.

Lots of ideas to implement
